#include "Ds18b20.h"

Ds18b20::Ds18b20(volatile uint8_t *pinsReg, uint8_t pin, uint8_t resolution/*=9*/, int8_t alarmHighTreshold/*=127*/, int8_t alarmLowTreshold/*=-128*/)

	:OneWire(pinsReg, pin)
{
	this->data[2]=alarmHighTreshold;
	this->data[3]=alarmLowTreshold;

	switch(resolution)
	{
		case 12:
			this->data[4]=0x7F;
			break;
		case 11:
			this->data[4]=0x5F;
			break;
		case 10:
			this->data[4]=0x3F;
			break;
		default:
			this->data[4]=0x1F;
			break;
	}
}

void Ds18b20::WriteScratchpad()
{
	this->DetectPresence();
	this->MatchRom();
	this->SendByte(WRITE_SCRATCHPAD);
	this->SendByte(this->data[2]);
	this->SendByte(this->data[3]);
	this->SendByte(this->data[4]);
}

void Ds18b20::CopyScratchpad()
{
	this->DetectPresence();
	this->MatchRom();
	this->SendByte(COPY_SCRATCHPAD);
	while (!this->ReadBit())
		;
}

void Ds18b20::RecallE2()
{
	this->DetectPresence();
	this->MatchRom();
	this->SendByte(RECALL_E2);
	while (!this->ReadBit())
		;
}

void Ds18b20::UpdateTemperature()
{
	this->DetectPresence();
	this->MatchRom();
	this->SendByte(START_CONVERTION);
	while (!this->ReadBit())
		;
}

void Ds18b20::UpdateAllTemperatures()
{
	this->DetectPresence();
	this->SkipRom();
	this->SendByte(START_CONVERTION);
	while (!this->ReadBit())
		;
}

bool Ds18b20::ReadScratchpad()
{
	int8_t i;
	uint64_t temp=0;
	uint8_t *tempPtr=(uint8_t *)&temp;
	this->DetectPresence();
	this->MatchRom();
	this->SendByte(READ_SCRATCHPAD);
	for (i=0; i<SCRATCHPAD_DIM; i++)
		this->data[i]=this->ReceiveByte();

	/* Calcolo la checksum dello scratchpad */
	for (i=SCRATCHPAD_DIM-2; i>=0; i--)
	{
		(*tempPtr) |= this->data[i];
		if(i==0)
			break;
		temp <<= 8;
	}

	/* Ritorno true se checksum OK, falso in caso contrario */
	return this->Crc8(data, SCRATCHPAD_DIM);
}


void Ds18b20::GetTemperature(int8_t &integer, uint16_t &fraction)
{
	fraction = this->data[0] & 0x000F; 

	switch(this->data[4])
	{
		case 0x1F: //Risoluzione 9 bit
			fraction >>= 3;
			fraction = 5*fraction; //Sposto 2^(-1) come bit meno significativo e moltiplico per 5 (1/2^1) => Esempio: Temperatura XXX.5
			break;
		case 0x3F: //Risoluzione 10 bit
			fraction >>= 2;
			fraction = 25*fraction; //Sposto 2^(-1) e 2^(-2) come bit meno significativi e moltiplico per 25 (1/2^2) => Esempio: Temperatura XXX.75
			break;
		case 0x5F: //Risoluzione 11 bit
			fraction >>= 1;
			fraction = 125*fraction;  //Sposto 2^(-1), 2^(-2) e 2^(-3)  come bit meno significativi e moltiplico per 125 (1/2^3) => Esempio: Temperatura XXX.625
			break;
		default: //Risoluzione 12 bit
			fraction = 625*fraction;   //Moltiplico per 625 (1/2^4) => Esempio: Temperatura XXX.5625
			break;
	}
	
	this->data[1] <<= 4;

	this->data[0] >>= 4;
	this->data[0] &= 0x0F;

	integer = this->data[0] | this->data[1];
}
