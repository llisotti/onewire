#include "OneWire.h"

OneWire::OneWire(volatile uint8_t *pinsReg, uint8_t pin)
{
	this->port=pinsReg; //Scelgo il pin sul quale montare il bus
	this->deviceId=0; //Resetto l'id del device

	/* Setto l'opportuna maschera */
	switch(pin)
	{
		case 1:
			this->mask=0x02;
			break;
		case 2:
			this->mask=0x04;
			break;
		case 3:
			this->mask=0x08;
			break;
		case 4:
			this->mask=0x10;
			break;
		case 5:
			this->mask=0x20;
			break;
		case 6:
			this->mask=0x40;
			break;
		case 7:
			this->mask=0x80;
			break;
		case 0:
		default:
			this->mask=0x01;
			break;		
	}

	this->alarm=false;	//Resetto lo stato di allarme
}

void OneWire::SendByte(uint8_t data)
{
	uint8_t temp;
	uint8_t i;
	
	for (i = 0; i < 8; i++)
	{
		temp = data & 0x01;
		if (temp)
			this->WriteBit1();
		else
			this->WriteBit0();
		
		data >>= 1;
	}
}

int8_t OneWire::ReceiveByte()
{
	uint8_t data=0;
	uint8_t i;
	
	for (i = 0; i < 8; i++)
	{
		data >>= 1;
		
		if (this->ReadBit())
			data |= 0x80;	
	}
	return data;
}

bool OneWire::DetectPresence()
{    
    /* Salvo SREG e disabilito gli interrupt */
	uint8_t temp;
    uint8_t sreg = SREG;
    cli();

    ONEWIRE_PULL_PIN_LOW(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_480);
    
    ONEWIRE_RELEASE_PIN(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_70);

    temp=ONEWIRE_READ_PIN(this->port, this->mask);
	_delay_us(ONEWIRE_DELAY_410);

	if (!temp)
	{
		/* Riporto il registro alla situazione iniziale */
		SREG=sreg;
		return true; //Ho letto 0 sul bus ovvero un dispositivo ha risposto
	} 
	else
	{
		/* Riporto il registro alla situazione iniziale */
		SREG=sreg;
		return false; //Ho letto 1 sul bus ovvero nessun dispositivo presente
	}
}

void OneWire::MatchRom()
{
	uint8_t *ptr=(uint8_t *)(&this->deviceId);
	uint8_t i;

	/* Invio il comando MATCH ROM */
	this->SendByte(ONEWIRE_MATCH);

	/* Invio l'id al quale � destinato il comando */
	for (i=8; i>0; i--, ptr++)
	{
		this->SendByte(*ptr);
	}
}

uint64_t OneWire::ReadRom()
{
	uint64_t data;
	uint8_t *ptr=(uint8_t *)(&data);
	uint8_t i;

	/* Invio il comando READ ROM */
	this->SendByte(ONEWIRE_READ);

	/* Invio l'id al quale � destinato il comando */
	for (i=8; i>0; i--, ptr++)
	{
		*ptr=this->ReceiveByte();
	}
	return data;
}

void OneWire::SkipRom()
{
	this->SendByte(ONEWIRE_SKIP);
}


bool OneWire::Crc8(int8_t *dataIn/*=0*/, const uint8_t dim/*=0*/)
{
	if (!dataIn) //Se non passo dati da calcolare la checksum, calcolo la checksum dell'id del device altrimenti...
	{
		uint8_t *devPtr, bit;
		uint64_t i, temp, seed=0;

		devPtr=(uint8_t *)&(this->deviceId);
		i=this->deviceId;
		devPtr += 7; //Punto alla checksum

		for (bit = 56; bit > 0; bit--)
		{
			temp = ((seed ^ i) & 0x0000000000000001);
			if (!temp)
				seed >>= 1;
			else
			{
				seed ^= 0x18;
				seed >>= 1;
				seed |= 0x80;
			}
			i >>= 1;
		}

		if (seed == (*devPtr))
			return true;
		else
			return false;
	} 
	else //...calcolo la checksum dei dati
	{
		uint8_t i, bit, temp, mydata, seed=0;

		for (i=0; i<dim-1; i++)
		{
			mydata=dataIn[i];
			for (bit=8; bit>0; bit --)
			{
				temp = ((seed ^ mydata) & 0x01);
				if (!temp)
					seed >>= 1;
				else
				{
					seed ^= 0x18;
					seed >>= 1;
					seed |= 0x80;
				}
				mydata >>= 1;
			}
		}

		i=dataIn[dim-1];
		if (seed == i)
			return true;
		else
			return false;
	}
}

/*
bool OneWire::Crc16(int16_t *dataIn, const uint8_t dim)
{
	uint8_t bit, index, temp;
	uint8_t thisData;
	uint16_t seed=0;

	for (index=0; index < dim-1; index++)
	{
		thisData=data[index];
		for (bit=8; bit>0; bit--)
		{
			temp = ((seed ^ thisData) & 0x01);
			if (temp == 0)
				seed >>= 1;
			else
			{
				seed ^= 0x4002;
				seed >>= 1;
				seed |= 0x8000;
			}
			thisData >>= 1;
		}
	}

	if (seed == data[index])
	{
		return true;
	}
	else
	{
		return false;
	}
}
*/

uint64_t OneWire::getId()
{
	return deviceId;
}

bool OneWire::getAlarmState()
{
	return alarm;
}

void OneWire::WriteBit0()
{
    /* Salvo SREG e disabilito gli interrupt */
    uint8_t sreg = SREG;
    cli();

    ONEWIRE_PULL_PIN_LOW(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_60);
    
    ONEWIRE_RELEASE_PIN(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_10);
    
    /* Riporto il registro alla situazione iniziale */
    SREG=sreg;	
}

void OneWire::WriteBit1()
{
    /* Salvo SREG e disabilito gli interrupt */
    uint8_t sreg = SREG;
    cli();

    ONEWIRE_PULL_PIN_LOW(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_6);
    
    ONEWIRE_RELEASE_PIN(this->port, this->mask);
    //_delay_us(ONEWIRE_DELAY_55);
	_delay_us(ONEWIRE_DELAY_64);
    
    /* Riporto il registro alla situazione iniziale */
    SREG=sreg;
}

uint8_t OneWire::ReadBit()
{
    /* Salvo SREG e disabilito gli interrupt */
	uint8_t temp;
	uint8_t sreg = SREG;
	cli();
    
    ONEWIRE_PULL_PIN_LOW(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_6);
    
     ONEWIRE_RELEASE_PIN(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_9);
    
    //*(this->bus) |= ONEWIRE_READ_PIN(this->bus, this->mask);
	temp=ONEWIRE_READ_PIN(this->port, this->mask);
    _delay_us(ONEWIRE_DELAY_55);
    
    /* Riporto il registro alla situazione iniziale */
    SREG=sreg;
    
    return temp;
}

int8_t SearchRom(OneWire **dev, int8_t numDevices)
{
	uint8_t lastDeviation=0, newDeviation=0;

	/* Comincio la ricerca */
	do
	{
		uint8_t bitIndex=1; //Indica la posizione del bit che sto esaminando
		uint64_t currentmask=0x01; //Maschera
		uint8_t bitA=0; //Primo bit campionato
		uint8_t bitB=0; //Secondo bit campionato (complemento del primo bit)

		/* Controllo che ci sia almeno un device sul bus e tolgo eventuali device dallo stato idle */
		if (!(*dev)->DetectPresence())
			return 0;

		/* Invio il comando SEARCH ROM */
		(*dev)->SendByte(ONEWIRE_ROM_SEARCH);

		/* Controllo la risposta */
		while (bitIndex <= 64)
		{
			bitA=(*dev)->ReadBit();
			bitB=(*dev)->ReadBit();

			/* bitA e bitB sono diversi */
			if (bitA ^ bitB)
			{
				/* Se bitA vale 1 tutti i device hanno questo bit ad 1... */
				if (bitA)
					((*dev)->deviceId) |= currentmask; //...lo scrivo nel device corrente
			}
			/* Se i bit sono entrambi 0 ci sono device che hanno 0 ad altri che hanno 1 come primo bit: devo scegliere */
			else if (!(bitA | bitB))
			{
				if(bitIndex == lastDeviation) //Se qui avevo fatto in precedenza una deviazione (scegliendo 0) ora scelgo 1
					((*dev)->deviceId) |= currentmask;
				else if (bitIndex > lastDeviation) //A questo bit ho altri device con bit differenti: scelgo sempre 0 (ovvero lascio 0) e...
					newDeviation=bitIndex; //... marco questo bit come prossima deviazione
				else if (!(((*dev)->deviceId) & currentmask))
					newDeviation=bitIndex;
				else
					;
			}
			/* Se i bit sono entrambi ad 1 allora errore ! */
			else
				return -1;
			

			/* Comunico la mia scelta ai devices */
			if (!(((*dev)->deviceId) & currentmask)) //Se ho scelto 0...
				(*dev)->WriteBit0(); //...comunico 0
			else
				(*dev)->WriteBit1(); //Altrimenti comunico 1
			
			/* Incremento il bit da elaborare ed aggiusto la maschera per agire su di esso */
			bitIndex++;
			currentmask <<= 1;
		}
		lastDeviation=newDeviation;
		newDeviation=0;
		numDevices--;
		if (!numDevices)
			break;
		dev++;
	}while(true);
	return lastDeviation;
}

 
uint8_t AlarmSearch(OneWire **dev, int8_t numDevices)
{
	uint8_t lastDeviation=0, newDeviation=0;
	OneWire **mybus;
	int8_t numDevs=numDevices; 
	uint8_t devsInAlarm=0;

	/* Azzero eventuali stati di allarme */
	for (; newDeviation < numDevices; newDeviation++) //Riciclo newDeviation
		dev[newDeviation]->alarm=false;

	newDeviation=0;

	/* Comincio la ricerca */
	do
	{
		
		uint8_t bitIndex=1; //Indica la posizione del bit che sto esaminando
		uint64_t currentmask=0x01; //Maschera
		uint64_t idInAlarm=0;
		uint8_t bitA=0; //Primo bit campionato
		uint8_t bitB=0; //Secondo bit campionato (complemento del primo bit)

		/* Controllo che ci sia almeno un device sul bus e tolgo eventuali device dallo stato idle */
		if (!(*dev)->DetectPresence())
			return 0;



		/* Invio il comando ALARM SEARCH */
		(*dev)->SendByte(ONEWIRE_ALARM_SEARCH);

		/* Controllo la risposta */
		while (bitIndex <= 64)
		{
			bitA=(*dev)->ReadBit();
			bitB=(*dev)->ReadBit();

			/* bitA e bitB sono diversi */
			if (bitA ^ bitB)
			{
				/* Se bitA vale 1 tutti i device hanno questo bit ad 1... */
				if (bitA)
					idInAlarm |= currentmask; //...lo scrivo nel device corrente
			}
			/* Se i bit sono entrambi 0 ci sono device che hanno 0 ad altri che hanno 1 come primo bit: devo scegliere */
			else if (!(bitA | bitB))
			{
				if(bitIndex == lastDeviation) //Se qui avevo fatto in precedenza una deviazione (scegliendo 0) ora scelgo 1
					idInAlarm |= currentmask;
				else if (bitIndex > lastDeviation) //A questo bit ho altri device con bit differenti: scelgo sempre 0 (ovvero lascio 0) e...
					newDeviation=bitIndex; //... marco questo bit come prossima deviazione
				else if (!(idInAlarm & currentmask))
					newDeviation=bitIndex;
				else
					;
			}
			/* Se i bit sono entrambi ad 1 non ci sono dispositivi in allarme */
			else
				return 0;
			

			/* Comunico la mia scelta ai devices */
			if (!(idInAlarm & currentmask)) //Se ho scelto 0...
				(*dev)->WriteBit0(); //...comunico 0
			else
				(*dev)->WriteBit1(); //Altrimenti comunico 1
			
			/* Incremento il bit da elaborare ed aggiusto la maschera per agire su di esso */
			bitIndex++;
			currentmask <<= 1;
		}
		lastDeviation=newDeviation;
		newDeviation=0; //Riciclo newDeviation
		mybus=dev;
		while (true)
		{
			if ((*mybus)->deviceId==idInAlarm && (*mybus)->alarm==false)
			{
				(*mybus)->alarm=true;
				devsInAlarm++;
			}
			newDeviation++;
			if (newDeviation == numDevs)
				break;
			mybus++;
		}
		numDevices--;
		newDeviation=0;
		if (!numDevices)
			break;
	}while(true);
	
	return devsInAlarm;
}