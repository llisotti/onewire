echo off
REM For doc generation, if you have a portable doxygen version set location variable with your proper path
SET location=D:\doxygen-1.8.13.windows.x64.bin\
IF EXIST "doxygen" (
	doxygen.exe Doxyfile
) ELSE (
	%location%doxygen.exe Doxyfile
)
pause