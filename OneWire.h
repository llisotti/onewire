/**
 * @mainpage Summary
 * @section Description
 * C++ library for build a Dallas/Maxim OneWire bus on an Atmel AVR gpio
 * @section Version
 * 100
 * @section Target
 * Atmega128A@8MHz - Build in Atmel Studio 7
 * @section Todo
 * @li Library deep test
 * @li Add 16 bit CRC calculator algorithm
 * @li Add support to parasitic supply
 * @li Add facility to create a OneWire bus over different gpio
 * @section License
 * Copyright (C) 2018 Luca Lisotti - luca.lisotti@yahoo.com

 *	This library is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

 *	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
	along with this library.  If not, see <http://www.gnu.org/licenses/>.

 * @section Usage
 * @include main.cpp
 */
#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include <util/delay.h>
#include <avr/interrupt.h>

/** @defgroup OneWire-Macro
 *  OneWire-Macro group
 *  @{
 */
//#define ONEWIRE_USE_INTERNAL_PULLUP /**< Uncomment if you want to use internal pull-up resistor (strongly discouraged) */

#define ONEWIRE_DELAY_480	(480)	/**< Delay of 480usec */
#define ONEWIRE_DELAY_6		(6)		/**< Delay of 6usec */
#define ONEWIRE_DELAY_9		(9)		/**< Delay of 9usec */
#define ONEWIRE_DELAY_64	(64)	/**< Delay of 64usec */
#define ONEWIRE_DELAY_60	(60)	/**< Delay of 60usec */
#define ONEWIRE_DELAY_70	(70)	/**< Delay of 70usec */
#define ONEWIRE_DELAY_10	(10)	/**< Delay of 10usec */
#define ONEWIRE_DELAY_55	(55)	/**< Delay of 55usec */
#define ONEWIRE_DELAY_410	(410)	/**< Delay of 410usec */

#define ONEWIRE_PULL_PIN_LOW(pinReg, bitMask) \
*(pinReg+1) |= bitMask; \
*(pinReg+2) &= ~bitMask;														/**< Pull bus to 0 */

#ifdef ONEWIRE_USE_INTERNAL_PULLUP //Se uso il pull-up interno al micro

#define ONEWIRE_RELEASE_PIN(pinReg, bitMask) \
*(pinReg+1) &= ~bitMask; \
*(pinReg+2) |=  bitMask;														/**< Release bus to high level */

#define ONEWIRE_READ_PIN(pinReg, bitMask) (((*(pinReg)) & (bitMask)) ? 1 : 0)	/**< Read and return pin value */

#else //Se NON uso il pull-up interno al micro

#define ONEWIRE_RELEASE_PIN(pinReg, bitMask) \
*(pinReg+1) &= ~bitMask; \
*(pinReg+2) &= ~bitMask;														/**< Release bus to high level */

#define ONEWIRE_READ_PIN(pinReg, bitMask) (((*(pinReg)) & (bitMask)) ? 1 : 0)	/**< Read and return pin value */
#endif

#define ONEWIRE_READ			0x33	/**< Read Rom command */
#define ONEWIRE_SKIP			0xCC	/**< Skip Rom command */
#define ONEWIRE_MATCH			0x55	/**< Match Rom command */
#define ONEWIRE_ROM_SEARCH		0xf0	/**< Search Rom command */
#define ONEWIRE_ALARM_SEARCH	0xEC	/**< Alarm Search command */

#define VERSION 100						/**< Library version */

/** @} */

/**
* @class OneWire
* @brief Onewire ROM function definitions
*/
class OneWire
{
	/**
	* @brief Send a SEARCH ROM command
	* @details Force devices enumeration over the OneWire bus
	* @param[in] dev Array of OneWire device pointers
	* @param[in] numDevices Number of devices to enumerate on the bus
	* @return 0 if correct and complete search; >0 if correct but no complete search; <0 if wrong search
	* @attention You have to enumerate your bus before use any other functions. Use this function before any other one
	*/
	friend int8_t SearchRom(OneWire **dev, int8_t numDevices);

	/**
	* @brief Send an ALARM SEARCH command
	* @details Find the devices in alarm over the OneWire bus
	* @param[in] dev Array of OneWire device pointers
	* @param[in] numDevices Number of devices to enumerate on the bus
	* @return number of devices in alarm, 0 otherwise 
	*/
	friend uint8_t AlarmSearch(OneWire **dev, int8_t numDevices);

	public:
	/**
	* @brief Constructor
	* @param[in] pinsReg Address to the PINx register over mounting a device
	* @param[in] pin Pin to the PINx register over mounting a device
	* @code
	* OneWire myDev(&PIND, PIND0); //Device mounted over PIN0 (0 to 7) of PORTD
	* @endcode
	*/
	OneWire(volatile uint8_t *pinsReg, uint8_t pin);

	/**
	* @brief Detect if any devices are presents
	* @return TRUE if any device is present on the bus, FALSE otherwise
	* @note Emit a reset signal. All devices came out from idle state
	*/
	bool DetectPresence();

	/**
	* @brief Send a MATCH ROM command
	*/
	void MatchRom();

	/**
	* @brief Send a READ ROM command
	* @return data read
	*/
	uint64_t ReadRom();

	/**
	* @brief Send a SKIP ROM command
	*/
	void SkipRom();

	/**
	* @brief CRC 8-bit calculator
	* @param[in] dataIn Address of first scratchpad byte
	* @param[in] dim Byte dimension of scratchpad
	* @return TRUE if checksum ok, FALSE otherwise
	* @note Omit all parameters if you want to check device rom id value
	* @code
	* mydev.Crc8(); //Check if device rom id checksum is corrected
	* ...
	* ...
	* //Example: data[0] to data[8] are 9-byte DS18B20 scratchpad; SCRATCHPAD_DIM=9 is size of scratchpad
	* mydev.Crc8(data, SCRATCHPAD_DIM); //Check if scratchpad data read are corrected
	* @endcode
	*/
	bool Crc8(int8_t *dataIn=0, const uint8_t dim=0);

	/**
	* @brief Get unique id of the device
	* @return 64-bit id value
	*/
	uint64_t getId();

	/**
	* @brief Get the state of device
	* @return true if device is in temperature threshold alarm, false otherwise
	*/
	bool getAlarmState();

	/**
	* @brief Send a byte of data
	* @param[in] data 8-bit data to be send
	*/
	void SendByte(uint8_t data);

	/**
	* @brief Receive a byte of data
	* @return 8-bit data read
	*/
	int8_t ReceiveByte();

	protected:
	/**
	* @brief Write 0 to device
	*/
	void WriteBit0();

	/**
	* @brief Write 1 to device
	*/
	void WriteBit1();

	/**
	* @brief Read one bit from bus
	* @return bit read
	*/
	uint8_t ReadBit();

	uint64_t deviceId;				/**< Id of device (64 bit) */
	bool alarm;						/**< State alarm indicator */

	private:
	volatile uint8_t *port;			/**< Address of the pins register over the device is mounted */
	uint8_t mask;					/**< Mask to select the real 1-Wire of the register over the device is mounted */
};

#endif /* ONEWIRE_H_ */