#ifndef DS18B20_H_
#define DS18B20_H_

#include "OneWire.h"

/** @defgroup Ds18b20-Macro
 *  Ds18b20-Macro group
 *  @{
 */
#define START_CONVERTION	0x44	/**< Temperature conversion command */
#define READ_SCRATCHPAD		0xBE	/**< Read RAM command */
#define WRITE_SCRATCHPAD	0x4E	/**< Write RAM command */
#define COPY_SCRATCHPAD		0x48	/**< Copy RAM content in EEPROM command */
#define RECALL_E2			0xB8	/**< Copy EEPROM content in RAM */

/**
* @brief Scratchpad fields of Ds18b20
* @details
* BYTE 0 - TEMPERATURE LSB\n
* BYTE 1 - TEMPERATURE MSB\n
* BYTE 2 - Hight temperature threshold register or user byte 1	<--> EEPROM storage/recall setting\n
* BYTE 3 - Low temperature threshold register or user byte 2	<--> EEPROM storage/recall setting\n
* BYTE 4 - Configuration register								<--> EEPROM storage/recall setting\n
* BYTE 5 - Reserved\n
* BYTE 6 - Reserved\n
* BYTE 7 - Reserved\n
* BYTE 8 - Crc
*/
#define SCRATCHPAD_DIM	9

/** @} */

/**
* @class Ds18b20
* @brief Dallas DS18B20 MEMORY function definitions
*/
class Ds18b20 : public OneWire
{
	public:
	/**
	* @brief Constructor
	* @param[in] pinsReg Address to the PINx register over mounting the Ds18b20
	* @param[in] pin Pin to the PINx register over mounting the Ds18b20
	* @param[in] resolution Resolution for temperature conversion. Only 9, 10, 11 or 12 bit possible values. Default value is 9 bit
	* @param[in] alarmHighTreshold if temperature go over this value the device go in alarm state. Default value is 127 �C
	* @param[in] alarmLowTreshold if temperature go lower this value the device go in alarm state. Default value is -128 �C
	* @code
	* Ds18b20 temp1(&PIND, PIND0, 9, 31);	//Mount a Ds18b20 over PIND0, with 9 bit convert resolution, high alarm value of 31 �C and low alarm value of 127 �C
	* Ds18b20 temp2(&PIND, PIND0, 10);		//Mount a Ds18b20 over PIND0, with 10 bit convert resolution, high alarm value of 127 �C and low alarm value of -127 �C
	* Ds18b20 temp3(&PIND, PIND0);			//Mount another Ds18b20 over PIND0, with 9 bit convert resolution, high alarm value of 127 �C and low alarm value of -127 �C
	* @endcode
	*/
	Ds18b20 (volatile uint8_t *pinsReg, uint8_t pin, uint8_t resolution=9, int8_t alarmHighTreshold=127, int8_t alarmLowTreshold=-128);

	/**
	* @brief Force a device to start a temperature conversion 
	*/
	void UpdateTemperature();

	/**
	* @brief Force all devices on the bus to start a temperature conversion
	* @note This function send a START CONVERTION COMMAND to all devices that support this command.
	*/
	void UpdateAllTemperatures();

	/**
	* @brief Read device scratchpad
	* @return true if scratchpad data are valid, false otherwise
	* @note You have to read device scratchpad after UpdateTemperature issue
	*/
	bool ReadScratchpad();

	/**
	* @brief Write scratchpad of the device
	*/
	void WriteScratchpad();

	/**
	* @brief Copy scratchpad of the device in EEPROM
	* @details This command make custom settings to be permanent in the device
	*/
	void CopyScratchpad();

	/**
	* @brief Copy EEPROM settings of the device in Scratchpad
	* @note This operation is automatically done after device power up
	*/
	void RecallE2();

	/**
	* @brief Get temperature value
	* @param[in, out] integer Integer part of temperature value
	* @param[in, out] fraction Fractional part of temperature value. This part is resolution value dependent
	*/
	void GetTemperature(int8_t &integer, uint16_t &fraction);

	private:
	int8_t data[SCRATCHPAD_DIM];	/**< Ds18b20 scratchpad array */
};


#endif /* DS18B20_H_ */