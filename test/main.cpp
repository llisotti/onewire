#include <avr/io.h>
#include <Ds18b20.h>

int main(void)
{
	sei();			//Enable interrupts
	DDRA |= 0x01;	//PA0 led out (my demo board)
	uint8_t ret=0;
	int8_t intVal1, intVal2, intVal3;
	uint16_t decVal1, decVal2, decVal3;

	Ds18b20 temp1(&PIND, PIND0, 9, 24);			//Mount a Ds18b20 over PIND0 with 9 bit convert resolution and high alarm value of 24 �C
	Ds18b20 temp2(&PIND, PIND0, 10, 127, -1);	//Mount a Ds18b20 over PIND0 with 10 bit convert resolution, high alarm value of 127 �C and low alarm value of -1 �C
	Ds18b20 temp3(&PIND, PIND0, 12);			//Mount another Ds18b20 over PIND0 with 12 bit convert resolution
	
	/* Mount OneWire bus */
	OneWire *bus[3]={&temp1, &temp2, &temp3};

	/* Overall OneWire bus enumeration */
	ret=SearchRom(bus, 3);

	/* Led off only if all devices checksum are corrected and the search is completed and corrected */
	if (temp1.Crc8() && temp2.Crc8() && temp3.Crc8() && !ret)
	{		
		PORTA &= 0xFE; //led OFF
	}
	else
	{
		PORTA |= 0x01; //led ON
	}

	_delay_ms(2000);
	

	/* Write Scratchpads of each device with custom settings */
	temp1.WriteScratchpad();
	temp2.WriteScratchpad();
	temp3.WriteScratchpad();
	while(true)
	{
		//temp1.CopyScratchpad(); //write resolution 9bit, high alarm 31�C in device EEProm

		/* Send convert command to all devices on the bus (SKIP ROM + CONVERT)... */
		temp1.UpdateAllTemperatures();

		/* ...or send single CONVERT COMMAND
		temp1.UpdateTemperature();
		temp2.UpdateTemperature();
		temp3.UpdateTemperature();
		*/

		/* Search devices in alarm */
		ret=AlarmSearch(bus, 3);

		/* if nothing device are in alarm maintain led OFF */
		if (ret)
		{
			PORTA |= 0x01; //led ON
		} 
		else
		{
			PORTA &= 0xFE; //led OFF
		}
		
		_delay_ms(2000);
		
		
		temp1.ReadScratchpad();
		temp2.ReadScratchpad();
		temp3.ReadScratchpad();
		
		/* If all ScratchPad read are corrected maintain led OFF */
		if (temp1.ReadScratchpad() && temp2.ReadScratchpad() && temp3.ReadScratchpad())
		{
			PORTA &= 0xFE; //led OFF
		}
		else
		{
			PORTA |= 0x01; //led ON
		}

		_delay_ms(2000);
		
		temp1.GetTemperature(intVal1, decVal1);
		temp2.GetTemperature(intVal2, decVal2);
		temp3.GetTemperature(intVal3, decVal3);
		temp1.GetTemperature(intVal1, decVal1);
	}
	return 0;
}